#include <stdio.h>
#define FIXEDCOST 500
#define VARIABLECOST 3

//function to calculate income
int income(int customers, int ticketprice){
	return customers * ticketprice;
}

//function to calculate expense
int expense(int customers){
	return FIXEDCOST + (VARIABLECOST * customers);
}

//function to calculate the Number of Customers
int noOfCustomers(int ticketprice){
	return (120-(ticketprice-15)/5*20);
}

//function to calculate profit
int profit(int ticketprice){
	int total = noOfCustomers(ticketprice);
	//calls the income and expense fucntions.
	return income(total,ticketprice) - expense(total);
}

int main(){
	//for loop to print all possible outcomes
	for (int ticketprice = 5; ticketprice < 50; ticketprice += 5){
		printf("The number of customers = %d, ticketprice = %d and profit = %d\n",noOfCustomers(ticketprice),ticketprice,profit(ticketprice));
	}
	return 0;
}
